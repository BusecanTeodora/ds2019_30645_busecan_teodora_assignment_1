export class Drug {
  iddrug: number;
  name: string;
  sideeffect: string;
  dosage: number;
}
