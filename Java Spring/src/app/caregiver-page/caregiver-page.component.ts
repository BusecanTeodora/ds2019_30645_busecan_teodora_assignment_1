import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Patient} from '../model/Patient';
import {CaregiverPageService} from '../services/caregiver-page.service';
import {Caregiver} from '../model/Caregiver';

@Component({
  selector: 'app-users',
  templateUrl: './caregiver-page.component.html',
  styleUrls: ['./caregiver-page.component.css']
})
export class CaregiverPageComponent implements OnInit {

  patients: Patient[] = [];
  caregiver: Caregiver;

  constructor(private router: Router, private caregiverPageService: CaregiverPageService) {
  }

  ngOnInit() {
    this.caregiver = JSON.parse(localStorage.getItem('user'));
    this.caregiverPageService.getPatients(this.caregiver.idcaregiver)
      .subscribe(data => {
        this.patients = data;
      });
  }


  logOut() {
    this.router.navigate(['']);
  }
}
