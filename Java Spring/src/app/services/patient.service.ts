import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {REST_API} from '../common/API';
import {Observable, throwError} from 'rxjs';
import 'rxjs-compat/add/operator/catch';
import {MatDialog} from '@angular/material';
import {ErrorService} from '../utils/error-service';
import {Patient} from '../model/Patient';


@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getPatients(): Observable<Patient[]> {
    return this.http.get<Patient[]>(REST_API + 'patient')
      .catch((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  insertPatient(patient: Patient): Observable<Patient> {
    return this.http.post<any>(REST_API + 'patient', patient)
      .catch((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  public deletePatient(patient: Patient) {
    return this.http.post<any>(REST_API + 'patient/delete', patient);
  }

  // getUserById(id: number): Observable<UserView> {
  //   return this.http.get<UserView>(REST_API + 'person' + id)
  //     .catch((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  // }
  public modifyPatient(patient: Patient) {
    return this.http.post<any>(REST_API + 'patient/modify', patient);
  }
}
