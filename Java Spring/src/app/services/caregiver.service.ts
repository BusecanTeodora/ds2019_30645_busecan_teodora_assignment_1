import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {REST_API} from '../common/API';
import {Observable, throwError} from 'rxjs';
import 'rxjs-compat/add/operator/catch';
import {MatDialog} from '@angular/material';
import {ErrorService} from '../utils/error-service';
import {Patient} from '../model/Patient';
import {Caregiver} from '../model/Caregiver';


@Injectable({
  providedIn: 'root'
})
export class CaregiverService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getCaregivers(): Observable<Caregiver[]> {
    return this.http.get<Caregiver[]>(REST_API + 'caregiver');
  }

  insertCaregiver(caregiver: Caregiver): Observable<Caregiver> {
    return this.http.post<any>(REST_API + 'caregiver', caregiver)
      .catch((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  public deleteCaregiver(caregiver: Caregiver) {
    return this.http.post<any>(REST_API + 'caregiver/delete', caregiver);
  }

  public modifyCaregiver(caregiver: Caregiver) {
    return this.http.post<any>(REST_API + 'caregiver/modify', caregiver);
  }
}
