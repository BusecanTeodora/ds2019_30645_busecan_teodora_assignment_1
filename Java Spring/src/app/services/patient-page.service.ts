import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {REST_API} from '../common/API';
import {Observable, throwError} from 'rxjs';
import 'rxjs-compat/add/operator/catch';
import {MatDialog} from '@angular/material';
import {ErrorService} from '../utils/error-service';
import {Patient} from '../model/Patient';


@Injectable({
  providedIn: 'root'
})
export class PatientPageService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getDetails(idPatient: number): Observable<Patient> {
    return this.http.post<Patient>(REST_API + '/patient/getPatient', idPatient);
  }
}
