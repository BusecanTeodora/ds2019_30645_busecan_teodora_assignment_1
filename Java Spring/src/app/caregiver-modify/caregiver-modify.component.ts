import { Component, OnInit } from '@angular/core';
import {PatientService} from '../services/patient.service';
import {Router} from '@angular/router';
import {Patient} from '../model/Patient';
import {Caregiver} from '../model/Caregiver';
import {CaregiverService} from '../services/caregiver.service';

@Component({
  selector: 'app-caregiver-modify',
  templateUrl: './caregiver-modify.component.html',
  styleUrls: ['./caregiver-modify.component.css']
})
export class CaregiverModifyComponent implements OnInit {
  caregiver: Caregiver;
  caregivers = [];

  constructor(private router: Router, private caregiverService: CaregiverService) { }

  ngOnInit() {
    this.caregiver = JSON.parse(localStorage.getItem('caregiver_modify'));
    console.log(this.caregiver);
  }
  confirmModify() {
    this.caregiverService.modifyCaregiver(this.caregiver).subscribe(data => this.router.navigate(['caregiver']));

  }


}
