import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Patient} from '../model/Patient';
import {PatientPageService} from '../services/patient-page.service';
import {Router} from '@angular/router';
import {Intake} from '../model/Intake';
import {IntakeService} from '../services/intake.service';

@Component({
  selector: 'app-users',
  templateUrl: './patient-page.component.html',
  styleUrls: ['./patient-page.component.css']
})
export class PatientPageComponent implements OnInit, AfterViewInit {
  patient: Patient;
  medicationPlans: Intake[];

  constructor(private router: Router, private intakeService: IntakeService) {
  }

  ngOnInit() {
    this.patient = JSON.parse(localStorage.getItem('user'));
    this.intakeService.getIntake(this.patient.idpatient)
      .subscribe(data => {
        this.medicationPlans = data;
        console.log(this.medicationPlans);
      });
  }
  ngAfterViewInit(): void {

  }

  logOut() {
    this.router.navigate(['']);
  }
}
