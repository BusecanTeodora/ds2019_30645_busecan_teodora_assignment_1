import { Component, OnInit } from '@angular/core';
import {Patient} from '../model/Patient';
import {IntakeService} from '../services/intake.service';
import {Intake} from '../model/Intake';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-medication-plan',
  templateUrl: './medication-plan.component.html',
  styleUrls: ['./medication-plan.component.css']
})
export class MedicationPlanComponent implements OnInit {

  medicationPlan: Intake;
  drugs: string;
  intervals: string;

  constructor(private intakeService: IntakeService, private router: Router) {
    // this.medication = new Medication();
    this.medicationPlan = new Intake();
  }

  ngOnInit() {
  }

  confirmMedicationPlan() {
    const intake: Intake = new Intake();
    intake.idintake = 6;
    intake.period = this.medicationPlan.period;
    intake.intake_intervals = this.intervals.split(',');
    intake.idPatient = this.medicationPlan.idPatient;
    intake.drugDTOs = this.drugs.split(',');


    console.log(intake);
    this.intakeService.addIntake(intake).subscribe(data => console.log(data));
    this.router.navigate(['/doctorPage']);
  }
}
