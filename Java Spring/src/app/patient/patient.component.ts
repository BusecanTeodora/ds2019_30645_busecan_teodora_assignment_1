import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Patient} from '../model/Patient';
import {PatientService} from '../services/patient.service';

@Component({
  selector: 'app-users',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  patients: Patient[] = [];
  patientInsert: Patient;
  registerForm: FormGroup;
  submitted: boolean;

  constructor(private router: Router, private patientService: PatientService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.submitted = false;
    this.updatePatientTable();
  }

  updatePatientTable() {
    this.registerForm = this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      birthday: ['', [Validators.required]],
      address: ['', Validators.required],
      gender: ['', Validators.required],
      idCaregiver: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.patientInsert = new Patient();
    this.patientService.getPatients()
      .subscribe(data => {
        this.patients = data;
      });
  }

  get requestedForm() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    this.patientInsert.name = this.registerForm.get('name').value;
    this.patientInsert.birthday = this.registerForm.get('birthday').value;
    this.patientInsert.address = this.registerForm.get('address').value;
    this.patientInsert.gender = this.registerForm.get('gender').value;
    this.patientInsert.idCaregiver = this.registerForm.get('idCaregiver').value;
    this.patientInsert.idUser = 1;
    this.patientInsert.username = this.registerForm.get('username').value;
    this.patientInsert.password = this.registerForm.get('password').value;
    this.patientService.insertPatient(this.patientInsert).subscribe(data => this.updatePatientTable());
  }

  deletePatient(id: number) {
    for (const patient of this.patients) {
      if (patient.idpatient === id) {
        console.log(patient);
        this.patientService.deletePatient(patient).subscribe(data => this.updatePatientTable());
      }
    }
  }

  modifyPatient(user: any) {
    localStorage.setItem('patient_modify', JSON.stringify(user));
    this.router.navigate(['patient/modifyPatient']);
  }
}
