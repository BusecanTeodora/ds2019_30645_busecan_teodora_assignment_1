//package com.example.springdemo.services;
//
//import com.example.springdemo.SpringDemoApplicationTests;
//import com.example.springdemo.dto.PatientDTO;
//import com.example.springdemo.dto.PatientViewDTO;
//import com.example.springdemo.entities.Patient;
//import com.example.springdemo.errorhandler.IncorrectParameterException;
//import com.example.springdemo.repositories.PatientRepository;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.jdbc.Sql;
//
//import java.util.List;
//
//public class PatientServiceTest extends SpringDemoApplicationTests {
//
//    @Autowired PatientService personService;
//
//
//
//    @Test(expected = IncorrectParameterException.class)
//    public void insertDTOBad() {
//        PatientDTO personDTO = new PatientDTO();
//        personService.insert(personDTO);
//
//    }
//    @Test
//    public void insertDTOGood() {
//        PatientDTO personDTO = new PatientDTO();
//        personDTO.setName("John Patterson");
//        personDTO.setAddress("George Baritiu nr. 22");
//        Integer id = personService.insert(personDTO);
//        assert(!personDTO.getName().equals("John Patterson"));
//
//    }
//    @Test
//    public void find(){
//        List<PatientDTO> patients = personService.findAll();
//        for(PatientDTO patientDTO: patients) {
//            assert (!patientDTO.getName().equals("ari"));
//        }
//
//    }
//}
