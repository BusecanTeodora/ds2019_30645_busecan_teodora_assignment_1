package com.example.springdemo.services;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public boolean login(String username, String password) {

        User user = userRepository.findByUsername(username);

        if (user.getUsername() == null || user.getPassword() == null) {

            return false;
        }
        if (user.getUsername().contentEquals(username) && user.getPassword().contentEquals(password)) {
            return true;
        }
        return false;
    }

    public int patient_doctor_or_caregiver(String username, String password) {
        if (!login(username, password)) {
            return Integer.parseInt(null);
        }
        List<User> users = userRepository.findAll();
        for (User u : users) {
            if (u.getUsername().contentEquals(username) && u.getPassword().contentEquals(password)) {
                return u.getUserType().getUserType();
            }
        }
        return Integer.parseInt(null);
    }

    public Integer findUserByUsername(String username) {
        User myUser = userRepository.findByUsername(username);
        return myUser.getIduser();
    }
}
