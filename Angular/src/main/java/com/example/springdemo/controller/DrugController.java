package com.example.springdemo.controller;


import com.example.springdemo.dto.DrugDTO;
import com.example.springdemo.services.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/drug")
public class DrugController {

    private final DrugService drugService;

    @Autowired
    public DrugController(DrugService drugService) {
        this.drugService = drugService;
    }

    @GetMapping(value = "/{iddrug}")
    public DrugDTO findById(@PathVariable("iddrug") Integer id){
        return drugService.finddrugById(id);
    }

    @GetMapping(value = "/drugs")
    public List<DrugDTO> findAll(){
        return drugService.findAll();
    }

    @PostMapping()
    public Integer insertDrugDTO(@RequestBody DrugDTO drugDTO){
        return drugService.insert(drugDTO);
    }

    @PostMapping("/modify")
    public Integer updateDrug(@RequestBody DrugDTO drugDTO) {
        return drugService.update(drugDTO);
    }

    @PostMapping(value = "/delete")
    public void deleteDrug(@RequestBody DrugDTO drugDTO){ drugService.delete(drugDTO);}

}
