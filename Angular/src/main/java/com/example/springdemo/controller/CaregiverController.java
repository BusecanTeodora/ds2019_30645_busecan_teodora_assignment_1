package com.example.springdemo.controller;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverDTOinsert;
import com.example.springdemo.dto.PatientDTOinsert;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{idcaregiver}")
    public CaregiverDTO findById(@PathVariable("idcaregiver") Integer id){
        return caregiverService.findcaregiverById(id);
    }

    @GetMapping()
    public List<CaregiverDTOinsert> findAll(){
        return caregiverService.findAll();
    }

    @PostMapping()
    public Integer insertCaregiverDTO(@RequestBody CaregiverDTOinsert caregiverDTOinsert){
        return caregiverService.insert(caregiverDTOinsert);
    }

    @PostMapping("/modify")
    public Integer updateCaregiver(@RequestBody CaregiverDTOinsert caregiverDTOinsert) {
        return caregiverService.update(caregiverDTOinsert);
    }

    @PostMapping(value = "/delete")
    public void deleteCaregiver(@RequestBody CaregiverDTOinsert caregiverDTOinsert){ caregiverService.delete(caregiverDTOinsert);}

    @PostMapping(value = "/getCaregiver")
    public CaregiverDTOinsert getCaregiver(@RequestBody String username){
        return caregiverService.findCaregiverByUsername(username);
    }

    @PostMapping(value = "/seePatients")
    public List<PatientDTOinsert> seePatients(@RequestBody Integer idCaregiver) {
        return caregiverService.seeAllPatients(idCaregiver);
    }
}
