package com.example.springdemo.controller;


import com.example.springdemo.dto.DrugDTO;
import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.UserLogIn;
import com.example.springdemo.services.DrugService;
import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/user")
public class LogInController {

    private final UserService userService;

    @Autowired
    public LogInController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping(value = "/login")
    public Integer logIn(@RequestBody UserLogIn userLogIn){
        boolean has_account = userService.login(userLogIn.getUsername(),userLogIn.getPassword());
        if (has_account == true){
            return userService.patient_doctor_or_caregiver(userLogIn.getUsername(),userLogIn.getPassword());
        }
        else {
            return 0;
        }
    }

}
