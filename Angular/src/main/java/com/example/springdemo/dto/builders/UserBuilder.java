package com.example.springdemo.dto.builders;


import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.UserTypeDTO;
import com.example.springdemo.entities.User;
import com.example.springdemo.entities.UserType;

public class UserBuilder {
    public UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User user){
        UserTypeDTO userTypeDTO = new UserTypeDTO();
        userTypeDTO.setUsertype(user.getUserType().getUserType());
        return new UserDTO(user.getIduser(),user.getUsername(),user.getPassword(),userTypeDTO);
    }

    public static User generateEntityFromDTO(UserDTO userDTO){
        UserType userType = new UserType();
        userType.setUserType(userDTO.getUserTypeDTO().getUsertype());
        return new User(userDTO.getIduser(),userDTO.getUsername(),userDTO.getPassword(), userType);
    }
}
