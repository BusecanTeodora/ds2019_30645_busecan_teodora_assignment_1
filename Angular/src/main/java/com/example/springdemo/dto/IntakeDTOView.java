package com.example.springdemo.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class IntakeDTOView {

    private Integer idintake;
    private String period;
    private List<String > intake_intervals;
    private Integer idPatient;
    private List<String > drugDTOs;

    public IntakeDTOView() {
    }

    public IntakeDTOView(Integer idintake, String period, List<String > intake_intervals, Integer idPatient, List<String> drugDTOs) {
        this.idintake = idintake;
        this.period = period;
        this.intake_intervals = intake_intervals;
        this.idPatient = idPatient;
        this.drugDTOs = drugDTOs;
    }

    public Integer getIdintake() {
        return idintake;
    }

    public void setIdintake(Integer idintake) {
        this.idintake = idintake;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public List<String> getIntake_intervals() {
        return intake_intervals;
    }

    public void setIntake_intervals(List<String > intake_intervals) {
        this.intake_intervals = intake_intervals;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public List<String> getDrugDTOs() {
        return drugDTOs;
    }

    public void setDrugDTOs(List<String> drugDTOs) {
        this.drugDTOs = drugDTOs;
    }
}